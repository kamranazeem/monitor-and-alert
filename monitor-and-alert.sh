#!/bin/bash


# Notes:
# ------
# * This script uses SSMTP, the configuration of which is only readable by root.
#     So, to be able to receive ALERT emails from the script, 
#     it should be run as user root.
# * If you run this script as regular user, the script will still work,
#     and dutifully log it's activity in system log,
#     but won't be able to send ALERT emails.
# * Required software: yum -y install netcat nmap-ncat ssmtp curl
# * When setting up cronjob, ensure to put the following two lines on top of crontab.
#   SHELL=/bin/bash
#   PATH=/usr/local/sbin:/usr/local/bin:/sbin:/bin:/usr/sbin:/usr/bin
#   The above two lines are very important.
#     If they are not present in crontab, many components of shell scripts do not work correctly,
#     especially if they use 'which' commands to figure out paths to various binaries.
#   The default PATH used by CRON is SHELL=/bin/sh, PATH=/usr/bin:/bin , 
#     which is not enough.



# --------------------------------- Start - User configurable variables ---------------


# FROM_ADDRESS
# This one will be used in the "From:" field in the email header.
# Note: In case you are using a gmail account for SMTP relay,
#         then remember that gmail SMTP server will always change 
#         the "From:" address to the gmail address you are using 
#         for smtp relay. 
# FROM_ADDRESS=monitor-bot@example.com
FROM_ADDRESS=monitor-bot@wbitt.com


# MONITOR_LIST_FILE
# CSV file with the following format:
# <curl|nc>, <Hostname|IP> , <port>, <recipient-email-1>  [<recipient-email-2> ...]
# Multiple recipients must not contain comma. Use space.
# Use full path for the CSV file.
# MONITOR_LIST_FILE=/usr/local/etc/monitor-and-alert-list.csv
MONITOR_LIST_FILE=/home/kamran/Projects/Personal/gitlab/monitor-and-alert/monitor-and-alert.csv


# SSMTP_CONFIG_FILE
# This script uses SSMTP to relay emails, 
#   so it is important to have full path of the ssmtp config file to use.
# This file has sensitive information in it,
#   therefore is only readable by root.
SSMTP_CONFIG_FILE=/etc/ssmtp/ssmtp.conf




# Generally you don't need to modify the following two options,
#   unless you know what you are doing! :)

# NETCAT_OPTIONS
# While trying to connect to Windows computers, '-z' is necessary.
#   So, we are forced to use netcat variant with '-z' support.
# If you don't have windows computers to monitor, 
#   and, you don't have '-z' option in your variant of netcat,
#   then you can remove '-z' from the NETCAT_OPTIONS below.
NETCAT_OPTIONS="-z -w 2"


# CURL_OPTIONS
# The following only gets the header data from the target,
#   which is more efficient, compared to getting the full web page.
# CURL_OPTIONS="-s -o /dev/null --connect-timeout 2 -I -w %{http_code}"
CURL_OPTIONS="-sL -o /dev/null --connect-timeout 2 -I"


# --------------------------------- Start - System variable assignments ----------------



NC=$(which nc)
NETCAT=$(which netcat)
CURL=$(which curl)
SSMTP=$(which ssmtp)




# ---------------------------------- Start - Function definitions -----------------------

function echolog() {
  echo "$1"
  echo "$1" | logger
}


function check_target() {
  if [ "${TEST_TYPE}" == "curl" ]; then  
    ${CURL} ${CURL_OPTIONS} ${REMOTE_ADDRESS}:${REMOTE_PORT}
  else
    ${NC} ${NETCAT_OPTIONS} ${REMOTE_ADDRESS} ${REMOTE_PORT} < /dev/null
  fi 
  echo $?
  
}


function send_email() {
  # Needs configuration in /etc/ssmtp/ssmtp.conf  
  # https://www.digitalocean.com/community/questions/how-to-send-emails-from-a-bash-script-using-ssmtp

  MESSAGE=$1

  EMAIL_FILE=$(mktemp)

  # Note: The empty echo lines before and after the MESSAGE (body) are VERY important.

  echo "From: ${FROM_ADDRESS}" > ${EMAIL_FILE}
  echo "To: $(echo ${RECIPIENTS} | tr ' ' ',')" >> ${EMAIL_FILE}
  echo "Subject: ${MESSAGE}" >> ${EMAIL_FILE}
  echo ""  >> ${EMAIL_FILE}
  echo "${MESSAGE}" >> ${EMAIL_FILE}
  echo ""  >> ${EMAIL_FILE}
  echo "------ End of alert message ------" >> ${EMAIL_FILE}
  echo ""  >> ${EMAIL_FILE}




  if [ -r ${SSMTP_CONFIG_FILE} ] ; then
    echo "Sending ALERT email to ${RECIPIENTS} ..."

    cat ${EMAIL_FILE} | ${SSMTP}  ${RECIPIENTS}
    
    rm -f ${EMAIL_FILE}
    
  else 
    echolog "WARNING: /etc/ssmtp/ssmtp.conf was not readable. Alert emails will not be sent."
  fi
}




function process_the_data() {
  REMOTE_CHECK_RESULT=$(check_target)
  # echo "DEBUG: REMOTE_CHECK_RESULT: ${REMOTE_CHECK_RESULT}"

  if [ ${REMOTE_CHECK_RESULT} -eq 0 ]; then
    echolog "Remote ${REMOTE_ADDRESS}:${REMOTE_PORT} is found as UP. Nothing to do."
  else
    echolog "Remote ${REMOTE_ADDRESS}:${REMOTE_PORT} is found as DOWN. Alerting recipients ${RECIPIENTS} ..."
    send_email "ALERT: Remote ${REMOTE_ADDRESS}:${REMOTE_PORT} is DOWN. Please investigate ..."
  fi  
  
}



# ---------------------------------- Start - Main program ------------------------------
echo
# Fix netcat first:
if [ -z "${NC}" ]; then
  # check if we have `netcat` binary install instead of `nc` binary.
  if [ -z "${NETCAT}" ]; then
    echo "None of the netcat binaries were found on this system."
    echo "You need to install nmap-ncat or netcat . exiting ..."
    exit 1
  else
    echo "'nc' binary not found, using 'netcat' binary ..."
    NC=${NETCAT}
  fi
fi

echo

MONITOR_LIST_TEMP_FILE=$(mktemp)

egrep -v "\#|^$" ${MONITOR_LIST_FILE} > ${MONITOR_LIST_TEMP_FILE}

while IFS=, read -r TEST_TYPE REMOTE_ADDRESS REMOTE_PORT RECIPIENTS
do
    echo "Processing: TEST_TYPE: ${TEST_TYPE} , REMOTE_ADDRESS: ${REMOTE_ADDRESS} ,  REMOTE_PORT: ${REMOTE_PORT} , RECIPIENTS: ${RECIPIENTS}"
    # process_the_data ${TEST_TYPE} ${REMOTE_ADDRESS} ${REMOTE_PORT} ${RECIPIENTS}
    process_the_data
    
done < ${MONITOR_LIST_TEMP_FILE}

echo
echo "Done! To watch script's activity in system logs, use: journalctl -xef"
echo

# ----------------------------------------------------------------------------------------
# NOTES:

# * nc  '-z' is very useful, but may not be present in some `netcat` variants.
#     In that case use:  nc -w 2 -v google.com 80 < /dev/null; echo $?
# * By default netcat in centos7 does not have '-z' option.
# * https://stackoverflow.com/questions/4922943/test-if-remote-tcp-port-is-open-from-a-shell-script
# * Need 'nc' , 'ssmtp' and 'esmtp-local-delivery' packages to be installed.
# * Windows firewall - as silly as it gets - does not like the standard netcat usage. 
#     It works only with netcat variants with '-z' option.


# If the /etc/ssmtp/ssmtp.conf file has:
#   FromLineOverride=NO
#   then, the received email will show that it came from 
#   the host OS root user "root <root@kworkhorse.home.wbitt.com>"

# If the /etc/ssmtp/ssmtp.conf file has:
#   FromLineOverride=YES
#   , and a "From: someone@example.com" line exists in the email body being sent through SSMTP
#   then, the received email will show that it came from "someone@example.com"

# If the /etc/ssmtp/ssmtp.conf file has:
#   FromLineOverride=YES
#   , but the "From:" line either does not exist, or "From:" is empty,
#   then, the received email will show that it came from 
#   the host OS root user "root <root@kworkhorse.home.wbitt.com>"


# Working configuration of ssmtp.conf file is shown below for reference:
# 
# [root@kworkhorse ~]# grep -v \# /etc/ssmtp/ssmtp.conf
# root=postmaster
# hostname=kworkhorse.home.wbitt.com
# mailhub=mail.example.com:465
# FromLineOverride=YES
# AuthMethod=LOGIN
# AuthUser=monitor-bot@example.com
# AuthPass=Some-Secret-Password
# UseTLS=YES
# TLS_CA_File=/etc/pki/tls/certs/ca-bundle.crt
# [root@kworkhorse ~]# 


